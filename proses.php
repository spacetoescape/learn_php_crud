<?php
include './fungsi.php';
session_start();

if (isset($_POST['aksi'])) {
  if ($_POST['aksi'] == "add") {
    $result = tambahData($_POST, $_FILES);

    if ($result['sql']) {
      $_SESSION['eksekusi'] = "Data Berhasil Ditambahkan";
      header("location: ./index.php");
      // echo "Data Berhasil Ditambahkan <a href='./index.php'>[Home]</a>";
    } else {
      echo $result['query'];
    }
  } else if ($_POST['aksi'] == "edit") {
    $result = ubahData($_POST, $_FILES);

    if ($result['sql']) {
      $_SESSION['eksekusi'] = "Data Berhasil Diubah";
      header("location: ./index.php");
      // echo "Ubah Data <a href='./index.php'>[Home]</a>";
    } else {
      echo $result['query'];
    }
  }
}

if (isset($_GET['hapus'])) {
  $result = hapusData($_GET);

  if ($result['sql']) {
    $_SESSION['eksekusi'] = "Data Berhasil Dihapus";
    header("location: ./index.php");
    // echo "Data Berhasil Dihapus <a href='./index.php'>[Home]</a>";
  } else {
    echo $result['query'];
  }
}
