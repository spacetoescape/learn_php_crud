-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 30, 2023 at 01:53 AM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sekolah`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_siswa`
--

CREATE TABLE `tb_siswa` (
  `id_siswa` int(11) NOT NULL,
  `nisn` varchar(15) NOT NULL,
  `nama_siswa` varchar(50) NOT NULL,
  `jenis_kelamin` varchar(15) NOT NULL,
  `foto_siswa` varchar(50) NOT NULL,
  `alamat` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `tb_siswa`
--

INSERT INTO `tb_siswa` (`id_siswa`, `nisn`, `nama_siswa`, `jenis_kelamin`, `foto_siswa`, `alamat`) VALUES
(1, '230123141601', 'Alexander', 'Laki-laki', 'img1.jpg', 'Jl. Imam Bonjol no.1'),
(2, '230123141602 |a', 'Bachtiar edited', 'Laki-laki', 'img2.jpg', 'Jl. Imam Bonjol no.212312'),
(6, '230123141605', 'Putri', 'Perempuan', '230123141605.png', 'Jl. Imam Bonjol no.5'),
(21, '2312314124', 'adsfasdfasdf', 'Perempuan', 'Screenshot (6).png', 'afqeqwedfshweqweqwe'),
(36, '3452345', 'sdfgwert', 'Perempuan', '3452345.png', 'vbncvbncvbn'),
(38, '23452345', 'egewetr', 'Laki-laki', '23452345.png', 'hfghdfh'),
(39, '47575467', 'gfdhdfhgdfgh', 'Laki-laki', '47575467.png', 'hfhwyerty'),
(40, '44563465', 'fghfhjgj', 'Laki-laki', '44563465.png', 'ghjgjgkkj'),
(41, '45647547457', 'bdhherte', 'Laki-laki', '45647547457.png', 'vbnvcbn'),
(42, '346343737', 'herhfghdhg', 'Laki-laki', '346343737.png', 'zcvafwwt'),
(43, '1234345324234', 'bsdfgserwet', 'Laki-laki', '1234345324234.png', 'xbsdfgsd');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_siswa`
--
ALTER TABLE `tb_siswa`
  ADD PRIMARY KEY (`id_siswa`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_siswa`
--
ALTER TABLE `tb_siswa`
  MODIFY `id_siswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
