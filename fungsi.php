<?php
  include './koneksi.php';

  function tambahData($data, $files) {
    $nisn = $data['nisn'];
    $namaSiswa = $data['nama_siswa'];
    $jenisKelamin = $data['jenis_kelamin'];

    $splitPhotoName = explode('.', $files['foto']['name']);
    $extension = $splitPhotoName[count($splitPhotoName) - 1];
    $foto = $nisn . '.' . $extension;

    $alamat = $data['alamat'];

    $dir = "img/";
    $tmpFile = $files['foto']['tmp_name'];

    move_uploaded_file($tmpFile, $dir . $foto);
    // renameFile($dir, $foto);

    $query = "INSERT INTO tb_siswa VALUES(null, '$nisn', '$namaSiswa', '$jenisKelamin', '$foto', '$alamat');";
    $sql = mysqli_query($GLOBALS['conn'], $query);

    $result = [
      'query' => $query,
      'sql' => $sql
    ];

    return $result;
  }

  function ubahData($data, $files) {
    $idSiswa = $data['id_siswa'];
    $nisn = $data['nisn'];
    $namaSiswa = $data['nama_siswa'];
    $jenisKelamin = $data['jenis_kelamin'];
    $alamat = $data['alamat'];

    $querySelect = "SELECT * FROM tb_siswa WHERE id_siswa = '$idSiswa'";
    $sqlSelect = mysqli_query($GLOBALS['conn'], $querySelect);
    $resultSelect = mysqli_fetch_assoc($sqlSelect);

    if ($files['foto']['name'] == "") {
      $foto = $resultSelect['foto_siswa'];
    } else {
      $tmpFile = $files['foto']['tmp_name'];
      $dir = "img/";
      $splitPhotoName = explode('.', $files['foto']['name']);
      $extension = $splitPhotoName[count($splitPhotoName) - 1];
      $foto = $resultSelect['nisn'] . '.' . $extension;
      unlink($dir . $resultSelect['foto_siswa']);
      move_uploaded_file($tmpFile, $dir . $foto);

      // renameFile($dir, $foto);
    }

    $query = "UPDATE tb_siswa SET nisn = '$nisn', nama_siswa = '$namaSiswa', jenis_kelamin = '$jenisKelamin', alamat = '$alamat', foto_siswa = '$foto' WHERE id_siswa = '$idSiswa';";
    $sql = mysqli_query($GLOBALS['conn'], $query);

    $result = [
      'query' => $query,
      'sql' => $sql
    ];

    return $result;
  }

  function hapusData($data) {
    $id_siswa = $data['hapus'];
    $querySelect = "SELECT * FROM tb_siswa WHERE id_siswa = '$id_siswa'";
    $sqlSelect = mysqli_query($GLOBALS['conn'], $querySelect);
    $result = mysqli_fetch_assoc($sqlSelect);
    unlink("img/" . $result['foto_siswa']);

    $query = "DELETE FROM tb_siswa WHERE id_siswa = '$id_siswa';";
    $sql = mysqli_query($GLOBALS['conn'], $query);

    $result = [
      'query' => $query,
      'sql' => $sql
    ];

    return $result;
  }

  function renameFile($dir, $fileName) {
    $fileCounter = new FilesystemIterator($dir); // count file in a directory
    $getFileInfo = pathinfo($dir . $fileName);
    $newFileName = "img" . iterator_count($fileCounter) . "." . $getFileInfo['extension'];
    rename($dir . $fileName, $dir . $newFileName);
  }

?>