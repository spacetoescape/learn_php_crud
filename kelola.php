<!DOCTYPE html>

<?php
include './koneksi.php';
// session_start();

$id_siswa = "";
$nisn = "";
$namaSiswa = "";
$jenisKelamin = "";
$alamat = "";

if (isset($_GET['ubah'])) {
  $id_siswa = $_GET['ubah'];

  $query = "SELECT * FROM tb_siswa WHERE id_siswa = '$id_siswa';";
  $sql = mysqli_query($conn, $query);

  $result = mysqli_fetch_assoc($sql);

  $nisn = $result['nisn'];
  $namaSiswa = $result['nama_siswa'];
  $jenisKelamin = $result['jenis_kelamin'];
  $alamat = $result['alamat'];
}
?>

<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <!-- Bootstrap -->
  <link href="./css/bootstrap/bootstrap.min.css" rel="stylesheet" />
  <script src="./js/bootstrap/bootstrap.bundle.min.js"></script>

  <!-- FontAwesome -->
  <link rel="stylesheet" href="./fonts/font-awesome-4.7.0/css/font-awesome.min.css" />

  <title>Belajar PHP - CRUD</title>
</head>

<body>
  <nav class="navbar navbar-light bg-light mb-4">
    <div class="container-fluid">
      <a class="navbar-brand" href="#"> CRUD BootStrap 5.1 </a>
    </div>
  </nav>

  <div class="container">
    <form method="POST" action="./proses.php" enctype="multipart/form-data">
      <input type="hidden" value="<?php echo $id_siswa; ?>" name="id_siswa">

      <div class="mb-3 row">
        <label for="nisn" class="col-sm-2 col-form-label">NISN</label>
        <div class="col-sm-10">
          <input required type="text" name="nisn" class="form-control" id="nisn" placeholder="Ex: 230123163001" value="<?php echo $nisn; ?>" />
        </div>
      </div>

      <div class="mb-3 row">
        <label for="nama" class="col-sm-2 col-form-label">Nama Siswa</label>
        <div class="col-sm-10">
          <input required type="text" name="nama_siswa" class="form-control" id="nama" placeholder="Ex: Upin" value="<?php echo $namaSiswa; ?>" />
        </div>
      </div>

      <div class="mb-3 row">
        <label for="jeniskelamin" class="col-sm-2 col-form-label">Jenis Kelamin</label>
        <div class="col-sm-10">
          <select required id="jeniskelamin" name="jenis_kelamin" class="form-select" aria-label="Default select example">
            <option <?php if ($jenisKelamin == 'Laki-laki') {
                      echo "selected";
                    } ?> value="Laki-laki">Laki-laki</option>
            <option <?php if ($jenisKelamin == 'Perempuan') {
                      echo "selected";
                    } ?> value="Perempuan">Perempuan</option>
          </select>
        </div>
      </div>

      <div class="mb-3 row">
        <label for="foto" class="col-sm-2 col-form-label">Foto Siswa</label>
        <div class="col-sm-10">
          <input <?php if (!isset($_GET['ubah'])) {
                    "required";
                  } ?> class="form-control" type="file" name="foto" id="foto" accept="image/*" />
        </div>
      </div>

      <div class="mb-3 row">
        <label for="alamat" class="col-sm-2 col-form-label">Alamat Lengkap</label>
        <div class="col-sm-10">
          <textarea required class="form-control" id="alamat" name="alamat" rows="3" placeholder="Ex: Jl. Milky Way no.1"><?php echo $alamat; ?></textarea>
        </div>

        <div class="mb-3 row mt-4">
          <div class="col">
            <?php
            if (isset($_GET['ubah'])) {
            ?>
              <button type="submit" name="aksi" value="edit" class="btn btn-primary">
                <i class="fa fa-floppy-o" aria-hidden="true"></i>
                Simpan Perubahan
              </button>
            <?php
            } else {
            ?>
              <button type="submit" name="aksi" value="add" class="btn btn-primary">
                <i class="fa fa-floppy-o" aria-hidden="true"></i>
                Tambahkan
              </button>
            <?php
            }
            ?>
            <a href="./index.php" type="button" class="btn btn-danger">
              <i class="fa fa-reply" aria-hidden="true"></i>
              Batal
            </a>
          </div>
        </div>
      </div>
    </form>
  </div>
</body>

</html>