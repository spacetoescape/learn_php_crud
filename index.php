<?php
include './koneksi.php';
session_start();

$query = "SELECT * from tb_siswa;";
$sql = mysqli_query($conn, $query);

$counter = 0;

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <!-- Bootstrap -->
  <link href="./css/bootstrap/bootstrap.min.css" rel="stylesheet" />
  <script src="./js/bootstrap/bootstrap.bundle.min.js"></script>

  <!-- FontAwesome -->
  <link rel="stylesheet" href="./fonts/font-awesome-4.7.0/css/font-awesome.min.css" />

  <!-- Datatables -->
  <link rel="stylesheet" type="text/css" href="./datatables/datatables.css" />
  <script type="text/javascript" src="./datatables/datatables.js"></script>

  <title>Belajar PHP - CRUD</title>
</head>

<script type="text/javascript">
  $(document).ready(function() {
    $('#dt').DataTable({
      "lengthMenu": [5, 10, 25, 50, 100]
    });
  });
</script>

<body>
  <nav class="navbar navbar-light bg-light">
    <div class="container-fluid">
      <a class="navbar-brand" href="#"> CRUD BootStrap 5.1 </a>
    </div>
  </nav>

  <!-- Judul -->
  <div class="container">
    <h1 class="mt-4">Data Siswa</h1>
    <figure>
      <blockquote class="blockquote">
        <p>Data yang telah di simpan pada Database</p>
      </blockquote>
      <figcaption class="blockquote-footer">
        CRUD <cite title="Source Title">Create Read Update Delete</cite>
      </figcaption>
    </figure>
    <a href="./kelola.php" type="button" class="btn btn-primary mb-3">
      <i class="fa fa-plus"></i>
      Tambah Data
    </a>

    <?php
    if (isset($_SESSION['eksekusi'])) :
    ?>

      <div class="alert alert-success alert-dismissible fade show" role="alert">
        <?php echo $_SESSION['eksekusi'] ?>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>

    <?php
      session_destroy();
    endif;
    ?>

    <div class="table-responsive">
      <table id="dt" class="table align-middle compact cell-border hover">
        <thead>
          <tr>
            <th>
              <center>No.</center>
            </th>
            <th>NISN</th>
            <th>Nama Siswa</th>
            <th>Jenis Kelamin</th>
            <th>Foto</th>
            <th>Alamat</th>
            <th>Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php
          while ($result = mysqli_fetch_assoc($sql)) {
          ?>
            <tr>
              <td>
                <center>
                  <?php echo ++$counter; ?>.
                </center>
              </td>
              <td>
                <?php echo $result['nisn']; ?>
              </td>
              <td>
                <?php echo $result['nama_siswa']; ?>
              </td>
              <td>
                <?php echo $result['jenis_kelamin']; ?>
              </td>
              <td>
                <img src="./img/<?php echo $result['foto_siswa']; ?>" alt="img1" style="width: 150px" />
              </td>
              <td>
                <?php echo $result['alamat'] ?>
              </td>
              <td>
                <a href="./kelola.php?ubah=<?php echo $result['id_siswa']; ?>" type="button" class="btn btn-success btn-sm">
                  <i class="fa fa-pencil"></i>
                </a>
                <a href="./proses.php?hapus=<?php echo $result['id_siswa']; ?>" type="button" class="btn btn-danger btn-sm" onclick="return confirm('Apakah anda yakin ingin menghapus data ini??')">
                  <i class="fa fa-trash"></i>
                </a>
              </td>
            </tr>
          <?php
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
  <div class="mb-5"></div>
</body>

</html>